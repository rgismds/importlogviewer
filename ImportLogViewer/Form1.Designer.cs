﻿namespace ImportLogViewer
{
    partial class frmImportLogViewer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listImportJobs = new System.Windows.Forms.ListBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.labelDb = new System.Windows.Forms.Label();
            this.dvImports = new System.Windows.Forms.DataGridView();
            this.labelImport = new System.Windows.Forms.Label();
            this.Close = new System.Windows.Forms.Button();
            this.labelLatestImports = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dvImports)).BeginInit();
            this.SuspendLayout();
            // 
            // listImportJobs
            // 
            this.listImportJobs.FormattingEnabled = true;
            this.listImportJobs.ItemHeight = 16;
            this.listImportJobs.Location = new System.Drawing.Point(171, 52);
            this.listImportJobs.Name = "listImportJobs";
            this.listImportJobs.Size = new System.Drawing.Size(302, 100);
            this.listImportJobs.TabIndex = 0;
            this.listImportJobs.SelectedIndexChanged += new System.EventHandler(this.listImportJobs_SelectedIndexChanged);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::ImportLogViewer.Properties.Resources.database_icon;
            this.pictureBox1.Location = new System.Drawing.Point(19, 48);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(116, 104);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 17;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // labelDb
            // 
            this.labelDb.AutoSize = true;
            this.labelDb.Location = new System.Drawing.Point(19, 23);
            this.labelDb.Name = "labelDb";
            this.labelDb.Size = new System.Drawing.Size(119, 17);
            this.labelDb.TabIndex = 16;
            this.labelDb.Text = "No DB connected";
            this.labelDb.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dvImports
            // 
            this.dvImports.AllowUserToAddRows = false;
            this.dvImports.AllowUserToDeleteRows = false;
            this.dvImports.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dvImports.Location = new System.Drawing.Point(22, 201);
            this.dvImports.MultiSelect = false;
            this.dvImports.Name = "dvImports";
            this.dvImports.ReadOnly = true;
            this.dvImports.RowTemplate.Height = 24;
            this.dvImports.Size = new System.Drawing.Size(919, 297);
            this.dvImports.TabIndex = 18;
            this.dvImports.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dvImports_CellContentClick);
            // 
            // labelImport
            // 
            this.labelImport.AutoSize = true;
            this.labelImport.Location = new System.Drawing.Point(168, 23);
            this.labelImport.Name = "labelImport";
            this.labelImport.Size = new System.Drawing.Size(81, 17);
            this.labelImport.TabIndex = 19;
            this.labelImport.Text = "Import Jobs";
            // 
            // Close
            // 
            this.Close.Location = new System.Drawing.Point(843, 527);
            this.Close.Name = "Close";
            this.Close.Size = new System.Drawing.Size(98, 30);
            this.Close.TabIndex = 20;
            this.Close.Text = "Close";
            this.Close.UseVisualStyleBackColor = true;
            this.Close.Click += new System.EventHandler(this.Close_Click);
            // 
            // labelLatestImports
            // 
            this.labelLatestImports.AutoSize = true;
            this.labelLatestImports.Location = new System.Drawing.Point(19, 181);
            this.labelLatestImports.Name = "labelLatestImports";
            this.labelLatestImports.Size = new System.Drawing.Size(97, 17);
            this.labelLatestImports.TabIndex = 21;
            this.labelLatestImports.Text = "Latest Imports";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(627, 48);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(200, 22);
            this.dateTimePicker1.TabIndex = 22;
            // 
            // frmImportLogViewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(973, 569);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.labelLatestImports);
            this.Controls.Add(this.Close);
            this.Controls.Add(this.labelImport);
            this.Controls.Add(this.dvImports);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.labelDb);
            this.Controls.Add(this.listImportJobs);
            this.Name = "frmImportLogViewer";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Import Log Viewer";
            this.Load += new System.EventHandler(this.frmImportLogViewer_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dvImports)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox listImportJobs;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label labelDb;
        private System.Windows.Forms.DataGridView dvImports;
        private System.Windows.Forms.Label labelImport;
        private System.Windows.Forms.Button Close;
        private System.Windows.Forms.Label labelLatestImports;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
    }
}

