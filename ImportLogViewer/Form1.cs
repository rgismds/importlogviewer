﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DatabaseConn;
using System.Data.SqlClient;

namespace ImportLogViewer
{
    public partial class frmImportLogViewer : Form
    {
        private SqlConnection oConn;
        static int nCallCount;

        public frmImportLogViewer()
        {
            InitializeComponent();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            DatabaseConn.DatabaseConn dbform;

            dbform = new DatabaseConn.DatabaseConn();
            dbform.ShowDialog();
            if (dbform.DialogResult == DialogResult.OK)
            {
                oConn = (SqlConnection)dbform.GetConnection();
                //this.Text = "Excel File Importer - " + oConn.Database;
                labelDb.Text = oConn.Database;
                LoadImportJobs();

            }
        }

        private void frmImportLogViewer_Load(object sender, EventArgs e)
        {

        }

        private void LoadImportJobs()
        {

                //oConn.Open();
                // use a SqlAdapter to execute the query
                using (SqlDataAdapter a = new SqlDataAdapter("SELECT DISTINCT ImportName FROM Import_Log", oConn))
                {
                    // fill a data table
                    var t = new DataTable();
                    a.Fill(t);

                    // Bind the table to the list box
                    listImportJobs.DisplayMember = "ImportName";
                    listImportJobs.ValueMember = "ImportName";
                    listImportJobs.DataSource = t;
                }

        }

        private void LoadImportsForJob(string sImportName)
        {

            nCallCount++;
            string sSql;
            if (oConn.State == ConnectionState.Closed)
                oConn.Open();

             // use a SqlAdapter to execute the query
             sSql = "SELECT * FROM Import_Log WHERE ImportName='" + sImportName + "' ORDER BY StartTime DESC";
             using (SqlDataAdapter a = new SqlDataAdapter(sSql, oConn))
             {
                 // fill a data table
                 var t = new DataTable();
                 a.Fill(t);
                 // Bind the table to the list box
                 dvImports.DataSource = t;
                 dvImports.AutoResizeColumns();
                 dvImports.Columns[0].Visible = false;
                 if (nCallCount == 1)
                    foreach (DataGridViewColumn c in dvImports.Columns)
                         c.HeaderText = ToSeparateWords(c.HeaderText);
             }
        }

        private string ToSeparateWords(string s)
        {
            string sReturn = "";

            foreach(char c in s)
            {
                if (char.IsUpper(c) )
                    sReturn+= " ";
                sReturn += c;

            }
            sReturn = sReturn.Trim();
            return sReturn;
        }

        private void listImportJobs_SelectedIndexChanged(object sender, EventArgs e)
        {
            string sImportJob;

            sImportJob = listImportJobs.SelectedValue.ToString();
            labelLatestImports.Text = "Latest Imports for " + sImportJob;

            LoadImportsForJob(sImportJob);
        }

        private void Close_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void dvImports_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
